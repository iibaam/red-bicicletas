var express = require('express')
var router = express.Router()
var bicicletaController = require('../controllers/bicicleta')

router.get('/', bicicletaController.bicicleta_list)
router.get('/create', bicicletaController.bicicletas_create_get)
router.post('/create', bicicletaController.bicicletas_create_post)
router.post('/:code/delete', bicicletaController.bicicletas_delete_post)
router.get('/:code/update', bicicletaController.bicicletas_update_get)
router.post('/:code/update', bicicletaController.bicicletas_update_post)
module.exports = router

