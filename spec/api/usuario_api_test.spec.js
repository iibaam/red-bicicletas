var mongoose = require('mongoose')
var Reserva = require('../../models/reserva')
var Bicicleta = require('../../models/bicicleta')
var Usuario = require('../../models/usuario')
var request = require('request')
var server = require('../../bin/www')
var base_url = 'http://localhost:3000/api/usuarios'


describe('Testing Usarios ', function() {
    
    beforeEach(function(done){        
        var mongoDB = "mongodb://localhost/testdb"
        mongoose.connect(mongoDB, { useNewUrlParser:true, useCreateIndex: true, useUnifiedTopology: true })

        const db = mongoose.connection
        db.on('error', console.error.bind(console, 'error al conectar a base de datos test'))
        db.once('open', function(){
            console.log('conectado a la base de datos de test')
            done()
        })
    })

    afterEach(function(done){
        Reserva.deleteMany({}, function(err, success) {
            if (err) console.log(err)
            Usuario.deleteMany({}, function( err, success ){
                if (err) console.log(err)
                Bicicleta.deleteMany({}, function(err, success)  {
                    if (err) console.log(err)
                    done()
                })
            })
        })
    })
    describe('POST USUARIOS /CREATE',()=>{
        it('status 200',(done)=>{
            const headers = {'content-type' : 'application/json'}
            let tUser = '{"nombre":"carlitos"}'

            request.post({
                url:base_url+'/create',
                headers: headers,
                body:tUser,
            },function(error,response,body){
                expect(response.statusCode).toBe(200)
                Usuario.find({"nombre":"carlitos"},(err,usuario) => {
                    if(err) console.log(err)
                    expect(usuario[0].nombre).toEqual('carlitos')
                    done()
                })
            })
        })
    })
    describe(' Reservas /reservar', () => {
        it('devuelve la información de la reserva', (done) => {
            
            let usuario = new Usuario({"nombre": "carlitos"})
            usuario.save();

            let bicicleta = new Bicicleta({"code": 1, "color":"roja","modelo":"playera"})
            bicicleta.save();

            const headers = {'content-type' : 'application/json'};
            
            let tReserva = {
                desde: "2020-12-9",
                hasta: "2020-08-10",
                id: usuario._id,
                bici_id: bicicleta._id
            };

            request.post({
                headers : headers,
                url : base_url+'/reservar',
                body : JSON.stringify(tReserva) //cambia js a json
            },
            function(error, response, body) {
                expect(response.statusCode).toBe(200);
                Reserva.find({}).populate('bicicleta').populate('usuario').exec(function(err, reservas) {
                    //if (err) console.log(err+'linea 81');                    
                    //expect(reservas[0].nombre).toEqual('carlitos');
                    done();
                });
                
            });
        
        });
    });


})