var mongoose = require('mongoose')
const { ExpectationFailed } = require('http-errors')
var bicicleta = require('../../models/bicicleta')
//beforeEach(() => {bicicleta.allBicis=[]})

describe('Testing Bicicletas', function(){
    beforeAll(function(done){
        var mongoDB = 'mongodb://localhost/testdb'
        mongoose.connect(mongoDB, { useNewUrlParser:true, useCreateIndex: true, useUnifiedTopology: true})

        const db = mongoose.connection
        db.on('error', console.error.bind(console, 'conection error'))
        db.once('open',function(){
            console.log('conectamos con la base de datos test!')
            done()
        })
        
    })


    afterEach(function(done){
        bicicleta.deleteMany({}, function(err,success){
            if (err) console.log(err)
            done()
        })
    })

    describe('Bicicleta.createInstance', () =>{
        it('crea una bicicleta', (done) =>{            
            var bici = bicicleta.createInstance(1,"verde","urbana", [-37.112232, -56.869642])        
            
            expect(bici.code).toBe(1)
            expect(bici.color).toBe("verde")
            expect(bici.modelo).toBe("urbana")
            expect(bici.ubicacion[0]).toEqual(-37.112232)
            expect(bici.ubicacion[1]).toEqual(-56.869642)
            done()
        })
    })

    describe('Bicicleta.allBicis',()=>{
        it('comienza vacia',(done) => {
            bicicleta.allBicis(function(err,bicis){
                expect(bicis.length).toBe(0)
                done()
            })
        })
    })

    describe('Bicicleta.add', () =>{
        it('agrega solo una bici',(done)=>{
            var aBici = new bicicleta({code:1, color:"verde", modelo:"urbana"})
            bicicleta.add(aBici, function(err, newBici){
                if (err) console.log(err)
                bicicleta.allBicis(function(err,bicis){
                    expect(bicis.length).toEqual(1)
                    expect(bicis[0].code).toEqual(aBici.code)
                    done()
                })
            })
        })
    })

    describe('bicicleta.findByCode', () =>{
        it('debe devovler la bicic con code 1', (done)=>{
            bicicleta.allBicis(function(err,bicis){
                
                expect(bicis.length).toBe(0)
                var aBici = new bicicleta({code:1, color:"verde",modelo:"urbana"})
                bicicleta.add(aBici, function(err, newBici){
                    if (err) console.log(err)
                    
                    var aBici2 = new bicicleta({code:2, color:"roja",modelo:"urbana"})
                    bicicleta.add(aBici2, function(err, newBici){
                        if (err) console.log(err)
                        bicicleta.findByCode(1, function (error,targetBici){
                            expect(targetBici.code).toBe(aBici.code)
                            expect(targetBici.color).toBe(aBici.color)
                            expect(targetBici.modelo).toBe(aBici.modelo)
                            
                            done()
                        })
                    })  
                })
            })
        })
    })
    describe('bicicleta.removeById', () =>{
        it('debe borrar la bicicleta con code 1', (done)=>{
            bicicleta.allBicis(function (err, bicis) {
                expect(bicis.length).toBe(0);
        
                var aBici = new bicicleta({code: 1, color: "rojo", modelo: "urbana"});
                bicicleta.add(aBici, function (err, newBici) {
                    if (err) console.log(err);
                    bicicleta.removeByCode(1, function (err) {
                        if (err) console.log(err);
                        bicicleta.allBicis(function (err, newBicis) {
                            expect(newBicis.length).toBe(0);
                            done()
                        })
                    })
                })
            })
        })
    })
})

/*describe('bicicleta.allBicis', () => {
    it('allbicis comenza vacio', () =>{
        expect(bicicleta.allBicis.length).toBe(0)
    })
})

describe('bicicleta.add', () => {
    it('agregamos una', () => {
        expect(bicicleta.allBicis.length).toBe(0)

        var a = new bicicleta(1, 'rojo', 'urbana', [-37.113035, -56.868813])
        bicicleta.add(a)

        expect(bicicleta.allBicis.length).toBe(1)
        expect(bicicleta.allBicis[0]).toBe(a)
    })
})

describe('bicicleta.findById', () => {
    it('deve devolver la bici con id 1', () => {
        expect(bicicleta.allBicis.length).toBe(0)

        var aBici = new bicicleta(1, 'rojo', 'urbana')
        var aBici2 = new bicicleta(2, 'verde', 'montaña')
        bicicleta.add(aBici)
        bicicleta.add(aBici2)

        
        var targetBici = bicicleta.findById(1)
        expect (targetBici.id).toBe(1)
        expect (targetBici.color).toBe(aBici.color)
        expect (targetBici.modelo).toBe(aBici.modelo)

    })
})

describe('bicicleta.removeById', () => {
    it('debe devolver 0 si se borra correctamente',()=>{
        expect(bicicleta.allBicis.length).toBe(0)
    
        var aBici = new bicicleta(3, 'rojo', 'urbana')
        bicicleta.add(aBici)

        expect(bicicleta.allBicis.length).toBe(1)
        
        bicicleta.removeById(3)
        expect(bicicleta.allBicis.length).toBe(0)
        
    })
    
})
*/