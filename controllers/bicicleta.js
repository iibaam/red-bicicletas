var bicicleta = require('../models/bicicleta.js')

exports.bicicleta_list = function(req, res){
    bicicleta.allBicis(function(error,result){
        res.render('bicicletas/index',{bicis: result})
    })
}
// Crear una nueva bicicleta
exports.bicicletas_create_get = function(req, res){
    res.render('bicicletas/create')
}

exports.bicicletas_create_post = function(req, res){
    var bici = new bicicleta({code:req.body.code, color:req.body.color, modelo:req.body.modelo,ubicacion: [req.body.lat, req.body.lon]})
    
    bicicleta.add(bici, function(err){
        res.redirect('/bicicletas')
    })
    
}
//borrar una bicicleta
exports.bicicletas_delete_post = function(req, res){
    bicicleta.removeByCode(req.body.code, function(err){
        res.redirect('/bicicletas')
    })
    
}
//actualizar una bicicleta
exports.bicicletas_update_get = function(req, res){
    var bici = bicicleta.findByCode(req.params.code,function(err,result){
        res.render('bicicletas/update',{bici:result})
    })
    
}

exports.bicicletas_update_post = function(req, res){
    var bici = bicicleta.findByCode(req.params.code,function(err,bici){
        bici.code = req.body.code
        bici.color = req.body.color
        bici.modelo = req.body.modelo
        bici.ubicacion = [req.body.lat, req.body.lon]
        bici.save()
        res.redirect('/bicicletas')
    })
    
}
