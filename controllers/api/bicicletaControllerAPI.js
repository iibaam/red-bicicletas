var bicicleta = require('../../models/bicicleta')

/*exports.bicicleta_list = function(req,res){
    res.status(200).json({
        bicicletas: bicicleta.allBicis
    })
}*/

exports.bicicleta_list = function (req,res){
    bicicleta.allBicis(function(err,bicis){
        res.status(200).json({bicis});
    })
}

exports.bicicleta_create = function(req,res){
    var bici = new bicicleta({
    code: req.body.code, 
    color: req.body.color, 
    modelo: req.body.modelo,
    ubicacion: [req.body.lat, req.body.lon]
    });

    bicicleta.add(bici);
    res.status(200).json({
        bicicleta: bici
    })
}

exports.bicicleta_delete = function(req,res){
    bicicleta.removeByCode(req.body.code, function(err){
        res.status(204).send()
    })
    
}

exports.bicicleta_update = function(req,res){
    var bici = bicicleta.findByCode(req.body.code,function(err){
        bici.code = req.body.code
        bici.color = req.body.color
        bici.modelo = req.body.modelo
        bici.ubicacion = req.body.ubicacion
        res.status(204).send()
    })
    
    
}